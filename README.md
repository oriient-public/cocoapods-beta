IPSFramework
This repository is a gateway to access **Beta** version of Oriient Indoor Positioning SDK via Cocoapods.
IPSFramework is a private framework. Therefore, to access IPSFramework, you have to provide credentials.
In the ~/.netrc file, add the following with your credentials (if a ~/.netrc file does not exist, create one:

machine cocoapods.oriient.me
login [SDK Access Username]
password [SDK Access Password]


Please contact support@oriient.me to request the credentials or if you have any other questions.
