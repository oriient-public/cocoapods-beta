Pod::Spec.new do |s|
    s.name = 'OriientSDK_dev_1'
    s.version = '6.0.1'
    s.summary = 'Oriient SDK provides access to all the utility and UX components provided by Oriient.'
    s.homepage = 'https://www.oriient.me'

    s.author = { 'Name' => 'dev@oriient.me' }
    s.license = { :type => 'Confidential', :file => 'OriientSDK.xcframework/LICENSE' }

    s.platform = :ios
    s.source = { :http => 'https://cocoapods.oriient.me/OriientSDK_dev_1/6.0.1.zip' }

    s.ios.deployment_target = '13.0'

    s.dependency 'IPSFramework_dev_1', '6.0.1'
    s.dependency 'IndoorMapSDK_dev_1', '6.0.1'

    s.ios.vendored_frameworks = 'OriientSDK.xcframework'
    s.swift_version = '5.0'
end
